package kz.aitu.chat.repository;

import kz.aitu.chat.model.Chat;
import org.springframework.data.jpa.repository.JpaRepository;
import java.util.List;

public interface ChatRepository extends JpaRepository<Chat, Long> {

}
